-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.7.19 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 test 的数据库结构
DROP DATABASE IF EXISTS `test`;
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test`;

-- 导出  表 test.holiday_time 结构
DROP TABLE IF EXISTS `holiday_time`;
CREATE TABLE IF NOT EXISTS `holiday_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `day` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='节假日作息时间';

-- 正在导出表  test.holiday_time 的数据：~0 rows (大约)
DELETE FROM `holiday_time`;
/*!40000 ALTER TABLE `holiday_time` DISABLE KEYS */;
INSERT INTO `holiday_time` (`id`, `uid`, `start`, `end`, `day`) VALUES
	(1, 1, '06:58:55', '18:59:11', '2017-01-01');
/*!40000 ALTER TABLE `holiday_time` ENABLE KEYS */;

-- 导出  表 test.shop_time 结构
DROP TABLE IF EXISTS `shop_time`;
CREATE TABLE IF NOT EXISTS `shop_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL DEFAULT '0',
  `start` time NOT NULL DEFAULT '00:00:00',
  `end` time NOT NULL DEFAULT '00:00:00',
  `week` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='运营时间表';

-- 正在导出表  test.shop_time 的数据：~6 rows (大约)
DELETE FROM `shop_time`;
/*!40000 ALTER TABLE `shop_time` DISABLE KEYS */;
INSERT INTO `shop_time` (`id`, `uid`, `start`, `end`, `week`) VALUES
	(1, 1, '10:00:00', '12:00:00', 0),
	(2, 1, '14:00:00', '20:00:00', 0),
	(3, 1, '10:00:00', '12:00:00', 1),
	(4, 1, '12:00:00', '14:00:00', 2),
	(5, 1, '10:00:00', '20:00:00', 3),
	(6, 1, '10:00:00', '20:00:00', 6);
/*!40000 ALTER TABLE `shop_time` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
