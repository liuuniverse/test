<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Exception;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getStatus(Request $request,Int $uid){
        try{
            $w=getdate()['wday'];
            $time=date('H:i:s');
            $step1=app('db')->select('select id from holiday_time where uid=:uid and day=:day',['uid'=>$uid,'day'=>date('Y-m-d')]);
            if(empty($step1)){
                $results = app('db')->select("SELECT * FROM shop_time where start < :time and end> :time and uid= :uid and week= :week",['time'=>$time,'uid'=>$uid,'week'=>$w]);
            }else{
                $results = app('db')->select("SELECT * FROM holiday_time where start < :time and end> :time and uid= :uid and day= :day",['time'=>$time,'uid'=>$uid,'day'=>date('Y-m-d')]);
            }
            if(empty($results)){
                return response()->json([
                    'status'=>'failed',
                    'code'=>0,
                    'message'=>['没有营业']
                ]);
            }else{
                return response()->json([
                    'status'=>'success',
                    'code'=>1,
                    'message'=>$results
                ]);
            }
        }catch(Exception $e){
            return response()->json([
                'status'=>'failed',
                'code'=>0,
                'message'=>['获取数据失败']
            ]);
        }
        
    }

    public function getTime(Request $request,Int $uid){
        try{
            $results=app('db')->select("select * from shop_time where uid=:uid order by week asc",['uid'=>$uid]);
            $arr=array();
            foreach($results as $value){
                if(array_key_exists($value->week,$arr)){
                    $long.=';'.$value->start.'-'.$value->end;
                }else{
                    $long=$value->start.'-'.$value->end;
                }
                $arr[$value->week]=array(
                    'name'=>$value->week,
                    'desc'=>'营业时间:'.$long,
                );
            }
            for($i=0;$i<7;$i++){
                $arr[$i]=$arr[$i] ?? ['name'=>$i,'desc'=>'休息'];
            }
            $weekname=['周日','周一','周二','周三','周四','周五','周六'];
            array_walk($arr,function(&$val)use($weekname){
                $val['name']=$weekname[$val['name']];
            });
            return response()->json([
                'status'=>'success',
                'code'=>1,
                'message'=>$arr
            ]);
        }catch(Exception $e){
            return response()->json([
                'status'=>'failed',
                'code'=>0,
                'message'=>['获取数据失败']
            ]);
        }
    }

    //
}
